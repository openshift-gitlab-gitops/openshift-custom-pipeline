FROM registry.access.redhat.com/ubi8/nodejs-12

WORKDIR /usr/src/app

COPY . /usr/src/app

ENV PORT 5000
EXPOSE 5000
CMD [ "node", "./bin/www" ]
