### GitOps for GitLab on Openshift

This project leverages a custom GitLab CI/CD flow to deploy a Node Express template application image to a target Openshift environment.

You will need to input the following CI/CD variables into the GitLab project to connect to your Openshift cluster:

- `$OC_URL` = API url of the Openshift instance you are attempting to connect to
- `$OC_TOKEN` =  *masked* personal or service account token of the account you are authenticating from
- `$OC_NAMESPACESLUG` = the base name of the projects you will be deploying your build config, image stream and deployment config to. You will need to create these projects using `oc create project $OC_NAMESPACESLUG-(dev, staging, prod)` to create the relevant projects/namespaces beforehand, or you can include this as an optional step in the GitLab CI file by adding a prebuild stage to verify whether these namespaces/projects exist and creating them if they do not already appear on the cluster.


### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
